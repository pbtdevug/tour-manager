/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.midp.lwuit.util;

import com.midp.lwuit.midlet.TourMIDlet;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import javax.microedition.io.ConnectionNotFoundException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.rms.RecordStore;
import org.kxml.Xml;
import org.kxml.io.ParseException;
import org.kxml.parser.ParseEvent;
import org.kxml.parser.XmlParser;

/**
 *
 * @author hudson
 */
public class SendPending implements Runnable {
    private TourMIDlet midlet;
    private InputStream is = null;
    private OutputStream os = null;
    private HttpConnection conn = null;
    public String status,pendingXML;
    private String confNum="",replyRootTag,upSignature,currentDateTime="",txNum;
    
    public SendPending(TourMIDlet midlet){
        this.midlet = midlet;
    }
    
    public void startConnection(){
        Thread th = new Thread(this);
        status = null;
        th.start();
    }
    
    public void setTransaction(String pending){
        this.pendingXML = pending;
        System.out.println(pendingXML);
    }
    
    public void run() {
        //String url = "http://localhost:8080/MarcsWeb/Tolls";
        String url = TourMIDlet.connectionUrl;
        int respCode;
        replyRootTag = "reply";
        byte [] bytes = pendingXML.getBytes();
        
        while(status == null){
            try {
                conn = (HttpConnection)Connector.open(url,Connector.READ_WRITE,true);
                conn.setRequestMethod(HttpConnection.POST);
                conn.setRequestProperty("User-Agent","Profile/MIDP-2.1 Confirguration/CLDC-1.1");
                conn.setRequestProperty("Content-Language", "en-CA");
                conn.setRequestProperty("Content-Type","text/xml");
                os = conn.openOutputStream();
                os.write(bytes, 0, bytes.length);//write transaction to outputstream
                try{
                    respCode = conn.getResponseCode();
                    System.out.println(respCode);
                }
                catch(ConnectionNotFoundException ex){
                    //ex.printStackTrace();
                    break;
                }
                catch(IOException ex){
                    //ex.printStackTrace();
                    break;
                }
                System.out.println(respCode);
                if (respCode == HttpConnection.HTTP_OK){
                    midlet.connectionIsAvailable = true;
                    is = conn.openInputStream();
                    /*int ch ;
                    StringBuffer sb = new StringBuffer();
                    while((ch = is.read())!=-1){
                        sb.append((char)ch);
                    }
                    System.out.println(sb.toString());*/
                        
                    viewXML(is);
                    
                    //check whether transaction was successful
                    if(status.equalsIgnoreCase("SUCCESSFUL")||respCode==200){
                        RecordStore pendingRecordStore = RecordStore.openRecordStore("pending", true);
                        //delete transaction from pending
                        pendingRecordStore.deleteRecord(midlet.pendingRecordID);
                        pendingRecordStore.closeRecordStore();
                        System.out.println("RecordID = "+midlet.pendingRecordID+" deleted");
                        break;
                    }
                    
                }
            }
            catch(ParseException ex){
                break;
            }
            catch (ConnectionNotFoundException ex) {
                //ex.printStackTrace();
                break;
            }
            catch (IOException ex) {
                //ex.printStackTrace();
                break;
            }
            catch (Exception ex) {
                //ex.printStackTrace();
                break;
            }
            finally{
                if(is!=null){
                    try {
                        is.close();
                    } 
                    catch (IOException ex) {
                    }
                }
                if(os!=null){
                    try {
                        os.close();
                    } 
                    catch (IOException ex) {
                    }
                }
                if(conn!=null){
                    try {
                        conn.close();
                    } 
                    catch (IOException ex) {
                    }
                }
            }
        }
    }
    
    public void viewXML(InputStream is){
        ParseEvent pe;
        try{
            InputStreamReader xmlReader = new InputStreamReader(is);
            XmlParser parser = new XmlParser( xmlReader );
            parser.skip();//skips the first line ie <?xml version='1.0' encoding='UTF-8'?>
            parser.read(Xml.START_TAG, null, replyRootTag);
            boolean trucking = true;
            while (trucking) {
                pe = parser.read();
                if (pe.getType() == Xml.START_TAG &&pe.getName().equals("transNo")) {
                    pe = parser.read();
                    txNum= pe.getText();
                }
                else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("confNo")) {
                    pe = parser.read();
                    confNum = pe.getText();
                }
                else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("status")) {
                    pe = parser.read();
                    status= pe.getText();
                    System.out.println("Status = "+status);
                }
                else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("dateTime")) {
                    pe = parser.read();
                    currentDateTime= pe.getText();
                }
                else if(pe.getType() == Xml.START_TAG&&pe.getName().equals("signature")){
                    pe = parser.read();
                    upSignature = pe.getText();
                }
                if (pe.getType() == Xml.END_TAG &&pe.getName().equals(replyRootTag))
                    trucking = false;
            }
        }
        catch(Exception ex){
        
        }
    }
}