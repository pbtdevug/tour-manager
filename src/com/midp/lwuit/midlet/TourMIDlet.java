/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.midp.lwuit.midlet;

import com.midp.lwuit.util.ConnectToServer;
import com.midp.lwuit.util.SendPending;
import com.midp.lwuit.util.Update;
import com.sun.lwuit.Command;
import com.sun.lwuit.Component;
import com.sun.lwuit.Dialog;
import com.sun.lwuit.Display;
import com.sun.lwuit.Form;
import com.sun.lwuit.Image;
import com.sun.lwuit.Label;
import com.sun.lwuit.Painter;
import com.sun.lwuit.Slider;
import com.sun.lwuit.TextField;
import com.sun.lwuit.animations.Transition;
import com.sun.lwuit.events.ActionEvent;
import com.sun.lwuit.events.ActionListener;
import com.sun.lwuit.geom.Dimension;
import com.sun.lwuit.layouts.BorderLayout;
import com.sun.lwuit.plaf.Border;
import com.sun.lwuit.plaf.UIManager;
import com.sun.lwuit.table.TableLayout;
import com.sun.lwuit.util.Resources;
import java.io.IOException;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import javax.microedition.lcdui.Canvas;
import javax.microedition.midlet.*;
import javax.microedition.rms.RecordEnumeration;
import javax.microedition.rms.RecordStore;
import javax.microedition.rms.RecordStoreException;
//import net.sf.microlog.core.Logger;
//import net.sf.microlog.core.LoggerFactory;
//import net.sf.microlog.core.PropertyConfigurator;


/**
 * @author hudson
 */
public class TourMIDlet extends MIDlet {
    private Painter diagonalPainter;
    private TextField f,f1;
    private Form splashScreen,pinForm,progressForm,waitForm;
    public Form state,checkPtForm,successForm;
    private Image imgSplash,progressImage;
    public Image alarmImage,errorImage,successImage,imgIcon;
    private TableLayout tableLayout;
    private Resources r;
    private Label imageLabel,pbtLabel,idLabel,iconLabel,waitLabel,otpLabel,dialogLabel;
    private TextField otpTextField,idTextField,pinTextArea;
    private Command submitCommand,cancelCommand,exitCommand,okCommand;
    public String pin,otp,id;
    public int pendingRecordID;
    public Timer timer;
    public StringBuffer activeTrxn = new StringBuffer();
    public Update up = new Update(this);
    private ConnectToServer cts = new ConnectToServer(this);
    private SendPending sp = new SendPending(this);
    public boolean connectionIsAvailable = false,historyFlag = false,pendingSendFlag =false,hisPendFlag = false;
    private Slider slider;
    private Dialog dialog;
    public String Version = "PBT-Saracen-10 Tue July 08 20:52:56 UTC 2014";
    public static final String connectionUrl = "http://www.peakbw.com/cgi-bin/igms_demo/saracen.mod.test";
    private int screenwidth,screenHeight;
    private boolean midletPaused = false;
    // A logger instance for this class
    //private static final Logger log = LoggerFactory.getLogger(HelloMidlet.class);

    public TourMIDlet(){
        //Configure Microlog
        //PropertyConfigurator.configure();
        //MidletPropertyConfigurator.configure();
    }
    
    public void startApp() {
        //log.info("Starting application");
        //init the LWUIT Display
        if(midletPaused){
            resumeMIDlet();
        }
        else{
            Display dis = Display.getInstance();
            Display.init(this);
        
            screenwidth = dis.getDisplayWidth();
            screenHeight = dis.getDisplayHeight();
        
            try {
                r = Resources.open("/splahresourceFile.res");
                alarmImage = r.getImage("alarm");
                errorImage = r.getImage("error");
                successImage = r.getImage("success");
                imgIcon = r.getImage("imgIcon");
                UIManager.getInstance().setThemeProps(r.getTheme("splashTheme"));
            } 
            catch (java.io.IOException e) {
                e.printStackTrace();
            }         
            try {
                getSplashScreen();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        pendingSheduler();
        midletPaused = false;
    }
    
    public void pauseApp() {
        //log.info("Pausing application");
        midletPaused = true;
    }
    
    public void destroyApp(boolean unconditional) {
        //log.info("Destroying application");
        // Shutdown Microlog gracefully
        //LoggerFactory.shutdown();
         //notifyDestroyed();
    }
    
//<editor-fold defaultstate="collapsed" desc=" splashScreen ">
    public void  getSplashScreen() throws IOException {
        //log.debug("splashScreen");
        splashScreen = new Form();
        splashScreen.setLayout(new BorderLayout());
        splashScreen.setTitle("IWMS");
        //splashScreen.addComponent(imageLabel());
        splashScreen.addComponent(BorderLayout.CENTER,pbtLabel());
        
        long startTime = System.currentTimeMillis();
        do{
            splashScreen.show();
        }
        while(System.currentTimeMillis()-startTime <= 3000);
        waitForm().show();
        up.startUpdate(pin);
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="imgSplash">
    private Image imgSplash(){
        if(imgSplash == null){
            imgSplash =  r.getImage("splashScreen");
        }
        return imgSplash;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="imgSplash">
    /*private Image imgIcon(){
        if(imgIcon == null){
            try{
                imgIcon = r.getImage("splashScreen");
            }
            catch(Exception ex){
                ex.printStackTrace();
            }
        }
        return imgIcon;
    }*/
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" splashScreen ">
    private Image progressImage(){
        if(progressImage == null){
            progressImage =  r.getImage("sandglass");
        }
        return progressImage;
    }
//</editor-fold>    
    
//<editor-fold defaultstate="collapsed" desc="pinForm">    
    public Form pinForm(){
        if(pinForm == null){
            pinForm = new Form();
            pinForm.setTitle("Enter Your 4 Digit PIN");
            pinForm.setLayout(new BorderLayout());
            pinForm.addComponent(BorderLayout.NORTH,pinTextArea());
            pinForm.addCommand(okCommand());
            pinForm.addCommand(exitCommand());
            pinForm.addGameKeyListener(Canvas.FIRE, new ActionListener() {
                        public void actionPerformed(ActionEvent evt) {
                            //if(evt.getSource() == Canvas.FIRE){
                                pin = pinTextArea.getText();
                                pinTextArea.setText("");
                                if(pin.length()!=4){
                                    //log.debug("PIN length != 4");
                                    //Dialog.show("Exception", e.getMessage(), "OK", null);
                                    //Dialog.setAutoAdjustDialogSize(true);
                                    //Dialog.
                                    dialog("PIN Error", "PIN must be 4 digits", cts.getTransition(),Dialog.TYPE_ERROR,alarmImage,5000);
                                    //Dialog.show("PIN Error", new Label("PIN must be 4 digits long"), null, Dialog.TYPE_ERROR, alarmImage, 5000,cts.getTransition());
                                }
                                else{
                                    //log.debug("Switch to CheckPtForm");
                                    checkPtForm().show();
                                }
                            //}
                        }
                    });
            pinForm.addCommandListener(new ActionListener() {
                        public void actionPerformed(ActionEvent evt) {
                            if(evt.getCommand() == okCommand){
                                //log.debug("User pressed the okCommand command.");
                                pin = pinTextArea.getText();
                                pinTextArea.setText("");
                                if(pin.length()!=4){
                                    //Dialog.show("Exception", e.getMessage(), "OK", null);
                                    //log.debug("PIN length != 4");
                                    //Dialog.setAutoAdjustDialogSize(true);
                                    dialog("PIN Error", "PIN must be 4 digits", cts.getTransition(),Dialog.TYPE_ERROR,alarmImage,5000);
                                    //Dialog.show("PIN Error", new Label("PIN must be 4 digits long"), null, Dialog.TYPE_ERROR, alarmImage, 5000,cts.getTransition());
                                }
                                else{
                                    //log.debug("Switch to CheckPtForm");
                                    checkPtForm().show();
                                }
                            }
                            else{
                                //log.debug("Exit Command Selected");
                                exitMIDlet();
                            }
                        }
                    });
        }
        return pinForm;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="checkPtForm">    
    public Form checkPtForm(){
        if(checkPtForm == null){
            checkPtForm = new Form();
            checkPtForm.setTitle("Staff Tour");
            checkPtForm.setSize(new Dimension(screenwidth,screenHeight));
            checkPtForm.setScrollable(true);
            checkPtForm.setScrollVisible(true);
            checkPtForm.setLayout(tableLayout());
            TableLayout.Constraint constraint = tableLayout.createConstraint(1,0);
            constraint.setHorizontalAlign(Component.CENTER);
            constraint.setHeightPercentage(12);
            checkPtForm.addComponent(constraint,otpLabel());
            constraint = tableLayout.createConstraint(2,0);
            //constraint.setHeightPercentage(14);
            constraint.setWidthPercentage(100);
            checkPtForm.addComponent(constraint,otpTextField());
            constraint = tableLayout.createConstraint(3,0);
            constraint.setHorizontalAlign(Component.CENTER);
            constraint.setHeightPercentage(12);
            checkPtForm.addComponent(constraint,idLabel());
            constraint = tableLayout.createConstraint(4,0);
            //constraint.setHeightPercentage(14);
            constraint.setWidthPercentage(100);
            checkPtForm.addComponent(constraint,idTextField());
            constraint = tableLayout.createConstraint(5,0);
            constraint.setHorizontalAlign(Component.CENTER);
            constraint.setHeightPercentage(50);
            checkPtForm.addComponent(constraint,iconLabel());
            checkPtForm.addCommand(submitCommand());
            checkPtForm.addCommand(cancelCommand());
            checkPtForm.addGameKeyListener(Canvas.FIRE, new ActionListener() {
                        public void actionPerformed(ActionEvent evt) {
                            otp = otpTextField.getText();
                            id = idTextField.getText();
                            idTextField.setText("");
                            otpTextField.setText("");
                            if(otp.length()!=6){
                                //Dialog.show(status, new Label(status), null, Dialog.TYPE_INFO, midlet.successImage, 5000,getTransition());
                                //Dialog.setAutoAdjustDialogSize(true);
                                dialog("OTP Error", "OTP must be 6 digits", cts.getTransition(),Dialog.TYPE_ERROR,alarmImage,5000);
                                //Dialog.show("OTP Error", new Label("OTP must be 6 digits long"), null, Dialog.TYPE_ERROR, alarmImage, 5000,cts.getTransition());
                            }
                            else{
                                if(id.length()!=4){
                                    //Dialog.setAutoAdjustDialogSize(true);
                                    dialog("CheckPtID Error", "ID must be 4 digits", cts.getTransition(),Dialog.TYPE_ERROR,alarmImage,5000);
                                    //Dialog.show("CheckPtID Error", new Label("CheckPtID must be 4 digits long"), null, Dialog.TYPE_ERROR, alarmImage, 5000,cts.getTransition());
                                }
                                else{
                                    state = checkPtForm;
                                    //String opt,String chkptID,String action,String desc, String pin,String transNum
                                    cts.setTransaction(otp, id, "", "", pin, getTrxnNumber());
                                    cts.startConnection();
                                    waitForm().setTitle("Submiting Tour");
                                    //waitLabel().setText("Connecting....");
                                    waitForm().showBack();
                                }
                            }
                        }
            });
            checkPtForm.addCommandListener(new ActionListener() {
                        public void actionPerformed(ActionEvent evt) {
                            if(evt.getCommand() == submitCommand){
                                //log.debug("Submit Command Selected");
                                otp = otpTextField.getText();
                                id = idTextField.getText();
                                idTextField.setText("");
                                otpTextField.setText("");
                                if(otp.length()!=6){
                                    //log.debug("OTP length != 6");
                                    //Dialog.show(status, new Label(status), null, Dialog.TYPE_INFO, midlet.successImage, 5000,getTransition());
                                    //Dialog.setCommandsAsButtons(true);
                                    dialog("OTP Error", "OTP must be 6 digits", cts.getTransition(),Dialog.TYPE_ERROR,alarmImage,5000);
                                    //Dialog.show("OTP Error", new Label("OTP must be 6 digits long"), null, Dialog.TYPE_ERROR, alarmImage, 5000,cts.getTransition());
                                }
                                else{
                                    if(id.length()!=4){
                                        //log.debug("PIN length != 4");
                                        //Dialog.setCommandsAsButtons(true);
                                        dialog("CheckPtID Error", "ID must be 4 digits", cts.getTransition(),Dialog.TYPE_ERROR,alarmImage,5000);
                                        //Dialog.show("OTP Error", new Label("CheckPtID must be 4 digits long"), null, Dialog.TYPE_ERROR, alarmImage, 5000,cts.getTransition());
                                    }
                                    else{
                                        //log.debug("Submitting Transaction ....");
                                        state = checkPtForm;
                                        //String opt,String chkptID,String action,String desc, String pin,String transNum
                                        cts.setTransaction(otp, id, "", "", pin, getTrxnNumber());
                                        cts.startConnection();
                                        waitForm().setTitle("Submiting Tour");
                                        //waitLabel().setText("Connecting....");
                                        waitForm().showBack();
                                    }
                                }
                            }
                            else{
                                //log.debug("Back or Cancel Command Selected");
                                pinForm().showBack();
                            }
                        }
                    });
            
        }
        return checkPtForm;
    }
//</editor-fold>    
    
//<editor-fold defaultstate="collapsed" desc="pinTextArea">    
    private TextField pinTextArea(){
        if(pinTextArea == null){
            pinTextArea = new TextField();
            pinTextArea.setVisible(true);
            pinTextArea.setEditable(true);
            pinTextArea.setEnabled(true);
            pinTextArea.setColumns(20);
            pinTextArea.setInputMode("123");
            //pinTextArea.setLabelForComponent(new Label("Enter Your Digit PIN"));
            pinTextArea.getStyle().setBgTransparency(100);
        }
        return pinTextArea;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="otpTextField">    
    private TextField otpTextField(){
        if(otpTextField == null){
            otpTextField = new TextField(TextField.NUMERIC);
            otpTextField.setVisible(true);
            otpTextField.setEditable(true);
            otpTextField.setEnabled(true);
            otpTextField.setHeight(1);
            otpTextField.setColumns(6);
            otpTextField.setLabelForComponent(otpLabel());
            otpTextField.setInputMode("123");
            otpTextField.getStyle().setBgTransparency(100);
        }
        return otpTextField;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="idTextField">    
    private TextField idTextField(){
        if(idTextField == null){
            idTextField = new TextField(TextField.NUMERIC);
            idTextField.setVisible(true);
            idTextField.setEditable(true);
            idTextField.setEnabled(true);
            idTextField.setHeight(1);
            idTextField.setInputMode("123");
            idTextField.setLabelForComponent(idLabel());
            idTextField.getStyle().setBgTransparency(100);
            idTextField.setColumns(6);
        }
        return idTextField;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="idLabel()">
    private Label idLabel(){
        if(idLabel == null){
            idLabel = new Label("CheckPtID");
            idLabel.getStyle().setFgColor(0xffffff);
            idLabel.getStyle().setBgTransparency(0);
            //idLabel.setSize(new Dimension(4,6));
        }
       return idLabel; 
    }
    
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="imageLabel">
    private Label imageLabel(){
        if(imageLabel == null){
            imageLabel = new Label(imgSplash());
            //imageLabel.getStyle().setFgColor(0xffffff);
            imageLabel.getStyle().setBorder(Border.createLineBorder(2));
            imageLabel.setAlignment(Component.CENTER);
        }
       return imageLabel; 
    }
    
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="iconLabel">
    private Label iconLabel(){
        if(iconLabel == null){
            iconLabel = new Label(imgIcon);
            //imageLabel.getStyle().setFgColor(0xffffff);
            iconLabel.getStyle().setBorder(Border.createLineBorder(2));
            iconLabel.setAlignment(Component.CENTER);
        }
       return iconLabel; 
    }
    
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="dialogLabel">
    private Label dialogLabel(Image img){
        //if(dialogLabel == null){
            dialogLabel = new Label(img);
            //imageLabel.getStyle().setFgColor(0xffffff);
            //dialogLabel.getStyle().setBorder(Border.createLineBorder(2));
            dialogLabel.setAlignment(Component.CENTER);
        //}
       return dialogLabel; 
    }
    
//</editor-fold>   
    
//<editor-fold defaultstate="collapsed" desc="otpLabel">
    private Label otpLabel(){
        if(otpLabel == null){
            otpLabel = new Label("OTP");
            otpLabel.getStyle().setBgTransparency(0);
            otpLabel.getStyle().setFgColor(0xffffff);
            //otpLabel.setShiftText(screenwidth);
            //otpLabel.setSize(new Dimension(4,6));
        }
       return otpLabel; 
    }
    
//</editor-fold> 
    
//<editor-fold defaultstate="collapsed" desc="waitLabel">
    private Label waitLabel(){
        if(waitLabel == null){
            waitLabel = new Label("Connecting ....");
            //otpLabel.getStyle().set3DText(true, true);
            waitLabel.getStyle().setBgTransparency(0);
            waitLabel.getStyle().setFgColor(0xffffff);
        }
       return waitLabel; 
    }
    
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="pbtLabel">
    private Label pbtLabel(){
        if(pbtLabel == null){
            pbtLabel = new Label(imgSplash());
            pbtLabel.setAlignment(Component.CENTER);
            pbtLabel.getStyle().setBgTransparency(0);
            pbtLabel.setText("Powered by PBT");
            pbtLabel.setTextPosition(Component.BOTTOM);
            pbtLabel.getStyle().setFgColor(0xffffff);

        }
       return pbtLabel; 
    }
    
//</editor-fold>    
   
//<editor-fold defaultstate="collapsed" desc=" cancelCommand ">
    private Command cancelCommand() {
        if (cancelCommand == null) {
            cancelCommand = new Command("Cancel", 1);
        }
        return cancelCommand;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="exitCommand">
    private Command exitCommand() {
        if (exitCommand == null) {
            exitCommand = new Command("Exit", 1);
        }
        return exitCommand;
    }
//</editor-fold>    
    
//<editor-fold defaultstate="collapsed" desc=" submitCommand ">
    private Command submitCommand() {
        if (submitCommand == null) {
            submitCommand = new Command("Submit", 0);
        }
        return submitCommand;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" okCommand ">
    private Command okCommand() {
        if (okCommand == null) {
           okCommand = new Command("Ok", 0);
        }
        return okCommand;
    }
//</editor-fold>   
    
//<editor-fold defaultstate="collapsed" desc=" exitMIDlet">
    public void exitMIDlet() {
        destroyApp(true);
        notifyDestroyed();
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="resumeMIDlet">
    public void resumeMIDlet() {
        pinForm().show();
    }
//</editor-fold>    
  
//<editor-fold defaultstate="collapsed" desc="successForm">    
    private Form successForm(){
        if(successForm == null){ 
            successForm = new Form("GuardTour");
        }
        return successForm;
    } 
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="waitForm">    
    private Form waitForm(){
        if(waitForm == null){ 
            waitForm = new Form("Checking for updates");
            waitForm.setLayout(new BorderLayout());
            waitForm.addComponent(BorderLayout.SOUTH,slider());
            waitForm.addComponent(BorderLayout.CENTER,waitLabel());
            waitForm.addCommand(cancelCommand());
            waitForm.addCommandListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    if(state!=null){
                        checkPtForm.show();
                    }
                    else{
                        pinForm().show();
                    }
                }
            });
        }
        return waitForm;
    } 
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="tableLayout">    
    private TableLayout tableLayout(){
        if(tableLayout == null){ 
            tableLayout = new TableLayout(5,1);
            tableLayout.getPreferredSize(checkPtForm);
        }
        return tableLayout;
    } 
//</editor-fold>    
    
//<editor-fold defaultstate="collapsed" desc="dialog">  
    //show("Error", new Label(status), null, Dialog.TYPE_INFO, null, 5000,getTransition());
    public void dialog(String title,String msg,Transition trans,int type,Image icon,int timeOut){
        //System.out.println("Dialog");
        //state.show();
        //if(dialog == null){ 
            //System.out.println("Dialog1");
            dialog = new Dialog("");
            //dialog.setSize(new Dimension(screenwidth/2,screenHeight/2));
            //dialog.getStyle().setBgTransparency(100);
            //dialog.getStyle().setBackgroundGradientEndColor(0xfb060e);
            dialog.setLayout(new BorderLayout());
            dialog.setScrollable(true);
            dialog.addComponent(BorderLayout.SOUTH,new Label(msg));
            //dialog.addComponent(BorderLayout.CENTER,new Label(icon));
            dialog.addComponent(BorderLayout.NORTH,dialogLabel(icon));
            //System.out.println("Dialog5");
            dialog.flushReplace();
            //System.out.println("Dialog6");
            dialog.setDialogType(type);
            dialog.setTransitionOutAnimator(trans);
            dialog.setTransitionInAnimator(trans);
            dialog.setTimeout(timeOut);
            //System.out.println("Dialog7");
        //}
        //dialog.show();    
        dialog.showPacked("Center", true);
        //dialog.show(9, 9, 1, 1, true);
    } 
//</editor-fold>    
    
//<editor-fold defaultstate="collapsed" desc="slider">    
    private Slider slider(){
        if(slider == null){ 
            slider = new Slider();
            slider.setInfinite(true);
            slider.animate();
            slider.getStyle().setBgTransparency(100);
            slider.setText("please wait");
            slider.setSmoothScrolling(true);
            slider.getStyle().setFgColor(0xffffff);
        }
        return slider;
    } 
//</editor-fold>
      
//<editor-fold defaultstate="collapsed" desc="createTourSuccessForm">    
    public void createTourSuccessForm(String today,String txNum, String chkPtID, String actn, String descrip,String status){
        try{
            successForm().removeAll();
            successForm.addComponent(new Label(today));
            
            //server connections fail
            if((connectionIsAvailable == false&&hisPendFlag==false)/*||(hisPendFlag == true&&historyFlag == false)*/){
                successForm.addComponent(new Label("Transaction Incomplete"));
            }
            //server connections successful
            else if(connectionIsAvailable == true&&hisPendFlag==false){
                successForm.addComponent(new Label("Transaction Successful"));
                successForm.addComponent(new Label("CONFIRMATION NUMBER: "+txNum));
            }
            //coming from pending or history transactions
            else{
                /*if(pendingFlag==true){
                    tourSuccessForm().append(new StringItem(null,"Transaction Incomplete"));
                    tourSuccessForm().append(new StringItem("TRXN NUMBER: "+confNumber,null));
                }else{
                    tourSuccessForm().append(new StringItem("STATUS: "+status,null));
                    tourSuccessForm().append(new StringItem("CONFIRMATION NUMBER: "+confNumber,null));
                }*/
            }
            //generic information on successForm
            successForm.addComponent(new Label("ACTION: "+actn));
            successForm.addComponent(new Label("DESCR: "+descrip));
            //tourSuccessForm().append(new StringItem(null,"Name: "+nemu+"\n"));
            
            //avoid saving transaction when from history or pending
            /*if(hisPendFlag==false){
                //save after successfuly connectining to server
                saveTransaction(activeTrxn.toString());
                tourSuccessForm().removeCommand(getBackCommand());
                tourSuccessForm().addCommand(newSubmissionCommand());
            }
            else{
                tourSuccessForm().removeCommand(newSubmissionCommand());
                tourSuccessForm().addCommand(getBackCommand());
            }
            switchDisplayable(null,tourSuccessForm());*/
            //log.debug("Switch to tourSuccessForm");
            successForm.show();
        }
        catch(Exception ex){
            //log.debug(ex.toString());
            Dialog.show(status, new Label(ex.getMessage()), null, Dialog.TYPE_INFO, null, 5000);
        }
    } 
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" split">
    public static Vector split(String splitStr, String delimiter) {
        //String [] splitArray;
            StringBuffer token = new StringBuffer();
            Vector tokens = new Vector();

            // split
            char[] chars = splitStr.toCharArray();
            for (int i=0; i < chars.length; i++) {
                if (delimiter.indexOf(chars[i]) != -1) {
                    // we bumbed into a delimiter
                    if (token.length() > 0) {
                        tokens.addElement(token.toString());
                        token.setLength(0);
                    }
                }
                else {
                    token.append(chars[i]);
                }
            }
            // don't forget the "tail"...
            if (token.length() > 0) {
                tokens.addElement(token.toString());
            }
            return tokens;
        }
    //</editor-fold>    
  
//<editor-fold defaultstate="collapsed" desc=" getDateTime">    
    public static String getDateTime() {//Fri 29 Nov 2013
        String mnt = "";
        String [] months = {"Jan","Feb","Mar", "Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct","Nov", "Dec"};
        Vector dateInfo = split((new Date()).toString()," ");
        if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[0])){
            mnt = "01";
        }
        else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[1])){
            mnt = "02";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[2])){
            mnt = "03";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[3])){
            mnt = "04";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[4])){
            mnt = "05";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[5])){
            mnt = "06";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[6])){
            mnt = "07";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[7])){
            mnt = "08";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[8])){
            mnt = "09";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[9])){
            mnt = "10";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[10])){
            mnt = "11";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[11])){
            mnt = "12";
        }
        String txn = dateInfo.elementAt(5).toString()+"-"+mnt+"-"+dateInfo.elementAt(2);
        return txn;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" getTrxnNumber">    
    public String getTrxnNumber() {
        String mnt = "";
        String [] months = {"Jan","Feb","Mar", "Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct","Nov", "Dec"};
        Vector dateInfo = split((new Date()).toString()," ");//Fri Nov 29 11:57:35 EAT 2013
        if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[0])){
            mnt = "01";
        }
        else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[1])){
            mnt = "02";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[2])){
            mnt = "03";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[3])){
            mnt = "04";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[4])){
            mnt = "05";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[5])){
            mnt = "06";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[6])){
            mnt = "07";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[7])){
            mnt = "08";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[8])){
            mnt = "09";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[9])){
            mnt = "10";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[10])){
            mnt = "11";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[11])){
            mnt = "12";
        }
        String txn = dateInfo.elementAt(2)+mnt+(dateInfo.elementAt(5).toString().substring(2, 4))+(dateInfo.elementAt(3).toString().substring(0, 2))+(dateInfo.elementAt(3).toString().substring(3, 5))+(dateInfo.elementAt(3).toString().substring(6, 8));
        return txn;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" saveTransaction"> 
    public void saveTransaction(String activeTrxin){
        try{
            RecordStore rst = RecordStore.openRecordStore("pending", true);
            int idt = rst.getNextRecordID();
            String transaction = idt+"^"+activeTrxin;
            byte [] bytes = transaction.getBytes();
            rst.addRecord(bytes, 0, bytes.length);
            rst.closeRecordStore();
            System.out.println(transaction);
        }
        catch(RecordStoreException ex){
            ex.printStackTrace();
        }
    }
//</editor-fold>  
    
//<editor-fold defaultstate="collapsed" desc=" pendingSheduler">    
    private void pendingSheduler() {
        timer = new Timer();
        try{
            //thread wakes up 1 min (60000) after the start of app to start sending pending transactions 
            timer.scheduleAtFixedRate(new TourMIDlet.SendPendingTask(), 60000,1*120000);
            // every after 2 min (120000) one pending transaction is sent to the server until they are all done
        }
        catch(IllegalStateException ex){
        }
        catch(IllegalArgumentException ex){
        }
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" SendPendingTask"> 
    class  SendPendingTask  extends TimerTask{
        public void run() {
            try{
                RecordStore pendingRecordStore = RecordStore.openRecordStore("pending", true);
                //when there are pending transactions
                System.out.println("In Pending......");
                if(!(pendingRecordStore.getNumRecords()==0)){
                    RecordEnumeration re = pendingRecordStore.enumerateRecords(null, null, false);
                    byte [] bytes = null;
                    while(re.hasNextElement()){
                        bytes = re.nextRecord();
                        break;
                    }
                    String trxn = new String(bytes,0,bytes.length);
                    Vector pendingTrxn = split(trxn,"^");
                    pendingRecordID = Integer.parseInt(pendingTrxn.elementAt(0).toString());
                    String pendingXML = pendingTrxn.elementAt(1).toString();
                    System.out.println("RecordID = "+pendingRecordID+" Sent");
                    sp.setTransaction(pendingXML);
                    sp.startConnection();
                }
                pendingRecordStore.closeRecordStore();
                //timer.cancel();
            }
            catch(Exception ex){
                //ex.printStackTrace();
            }
        }
    
    }
//</editor-fold>    
    
}