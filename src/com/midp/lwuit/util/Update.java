/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.midp.lwuit.util;

import com.midp.lwuit.midlet.TourMIDlet;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Hashtable;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import javax.microedition.io.ConnectionNotFoundException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.rms.RecordEnumeration;
import javax.microedition.rms.RecordStore;
import org.kxml.Xml;
import org.kxml.io.ParseException;
import org.kxml.parser.ParseEvent;
import org.kxml.parser.XmlParser;

/**
 *
 * @author hudson
 */
public class Update implements Runnable{
    private TourMIDlet midlet;
    private OutputStream os = null;
    private InputStream is = null,fis = null;
    private HttpConnection conn = null;
    private String userId ="PBT-P41",currentDateTime;
    public String today = TourMIDlet.getDateTime(),updateSign,pin,upSignature;
    public Vector codes,unitsVector = new Vector();
    public Hashtable guardHashtable = new Hashtable(),locationsHashTable = new Hashtable(),itemsHashtable = new Hashtable();
    private boolean guardStoreIsEmpty;
    public boolean firstUpdate = true;
    //private FileConnection fc = null;
    private Timer updateTimer;
    
    public Update(TourMIDlet mid){
        this.midlet = mid;
    }
    
    public void startUpdate(String pin){
        this.pin = pin;
        this.updateSign = decodeGuardRecords();
        upSignature = null;
        Thread t = new Thread(this);
        t.start();//starts run() or new thread
        updateTaskSheduler();
        
    }
    
    private String createUpdateXML(String id,String upSign,String pin){
        StringBuffer xmlStr = new StringBuffer();
        xmlStr.append("<?xml version='1.0' encoding='UTF-8'?>");
        xmlStr.append("<updateReq>");
        xmlStr.append("<id>").append(id).append("</id>");
        xmlStr.append("<pin>").append(pin).append("</pin>");
        xmlStr.append("<signature>").append(upSign).append("</signature>");
        xmlStr.append("<version>").append(midlet.Version).append("</version>");
        xmlStr.append("</updateReq>");
        //System.out.println(xmlStr.toString());
        return xmlStr.toString();
    }
    
    public void run() {
        //String url = "http://localhost:8080/MarcsWeb/Update";
        //String url = "http://www.peakbw.com/cgi-bin/secplus/server2.test";
        String url = TourMIDlet.connectionUrl;
        System.out.println(url);
        int respCode;
        while(upSignature == null){
            try {
                conn = (HttpConnection)Connector.open(url,Connector.READ_WRITE,true);
                conn.setRequestMethod(HttpConnection.POST);
                conn.setRequestProperty("User-Agent","Profile/MIDP-2.1 Confirguration/CLDC-1.1");
                conn.setRequestProperty("Content-Language", "en-CA");
                conn.setRequestProperty("Content-Type","text/xml");
                //open stream connection
                os = conn.openOutputStream();
                byte [] bytes = createUpdateXML(userId,updateSign,pin).getBytes();
                os.write(bytes, 0, bytes.length);
                try{
                    respCode = conn.getResponseCode();
                    System.out.print(respCode);
                }
                catch(ConnectionNotFoundException ex){
                    //ex.printStackTrace();
                    updateTimer.cancel();
                    upSignature = "";
                    System.out.println(guardStoreIsEmpty);
                    if(guardStoreIsEmpty==true){
                        //loadGuardListFromFileSystem();
                        //midlet.switchDisplayable(null, midlet.mainMenu());
                    }
                    else{
                         //midlet.switchDisplayable(null, midlet.mainMenu());
                    }
                    break;
                }
                catch(IOException ex){
                    updateTimer.cancel();
                    upSignature = "";
                    System.out.println(guardStoreIsEmpty);
                    if(guardStoreIsEmpty==true){
                        //loadGuardListFromFileSystem();
                        //midlet.switchDisplayable(null, midlet.mainMenu());
                    }
                    else{
                         //midlet.switchDisplayable(null, midlet.mainMenu());
                    }
                    break;
                }
                if(respCode == HttpConnection.HTTP_OK){//successful connection to server
                    midlet.connectionIsAvailable = true;
                    is= conn.openDataInputStream();
                    /*int ch ;
                    StringBuffer sb = new StringBuffer();
                    while((ch = is.read())!=-1){
                        sb.append((char)ch);
                    }
                    System.out.println(sb.toString());*/
                    viewXML(is);
                    updateTimer.cancel();
                }
                else{//server error
                    upSignature = "";
                    updateTimer.cancel();
                    midlet.connectionIsAvailable = false;
                    if(guardStoreIsEmpty==true){
                        //loadGuardListFromFileSystem();
                        //midlet.switchDisplayable(null, midlet.mainMenu());
                    }
                    else{
                         //midlet.switchDisplayable(null, midlet.mainMenu());
                    }
                    break;
                    /*midlet.getAlert().setTitle("Connection Error");
                    midlet.getAlert().setString("Network Unavailable");
                    midlet.switchDisplayable(null,midlet.getAlert());*/
                }
            }
            catch(ParseException ex){
                updateTimer.cancel();
                break;
            }
            catch (ConnectionNotFoundException ex){// can't access server
                //ex.printStackTrace();
                updateTimer.cancel();
                if(guardStoreIsEmpty==true){
                    //loadGuardListFromFileSystem();
                    //midlet.switchDisplayable(null, midlet.mainMenu());
                }
                else{
                    //midlet.switchDisplayable(null, midlet.mainMenu());
                }
                /*midlet.switchDisplayable(null,midlet.getAlert());
                midlet.getAlert().setTitle("I/O Error");
                midlet.getAlert().setString("I/O failure");*/
                midlet.connectionIsAvailable = false;
                break;
            }
            catch (IOException ex) {
                //ex.printStackTrace();
                updateTimer.cancel();
                if(guardStoreIsEmpty==true){
                    //loadGuardListFromFileSystem();
                    //midlet.switchDisplayable(null, midlet.mainMenu());
                }
                else{
                    //midlet.switchDisplayable(null, midlet.mainMenu());
                }
                /*midlet.switchDisplayable(null,midlet.getAlert());
                midlet.getAlert().setTitle("I/O Error");
                midlet.getAlert().setString("I/O failure");*/
                midlet.connectionIsAvailable = false;
                break;
            }
            catch (Exception ex) {
                //ex.printStackTrace();
                /*midlet.switchDisplayable(null,midlet.getAlert());
                midlet.getAlert().setString(ex.getMessage());*/
                //midlet.connectionIsAvailable = false;
                break;
            }
            finally{
                if(os!=null&&conn!=null&&is!=null){
                    try {
                        is.close();
                        os.close();
                        conn.close();
                    } 
                    catch (IOException ex) {
                        /*ex.printStackTrace();
                        midlet.switchDisplayable(null,midlet.getAlert());
                        midlet.getAlert().setString(ex.getMessage());*/
                    }
                }
                else{
                   try {
                        os.close();
                        conn.close();
                    } 
                    catch (IOException ex) {
                        /*ex.printStackTrace();
                        midlet.switchDisplayable(null,midlet.getAlert());
                        midlet.getAlert().setString(ex.getMessage());*/
                    } 
                }
                break;
            }
        }
        midlet.pinForm().show();
    }
    
    private String decodeGuardRecords(){
        String udateSignature="";
        Vector id_Guard,lctn_officer,id_item;
        try{
            RecordStore rs = RecordStore.openRecordStore("guardRecords", true);
            if((rs.getNumRecords()>0)){//check wether recordStore in not empty
                RecordEnumeration rEnum = rs.enumerateRecords(null, null, false);// creates enumeration of records
                //byte [] bts = null;
                //while(rEnum.hasNextElement()){
                byte [] bts = rEnum.nextRecord();
                //break;
                //}
                String records = new String(bts,0,bts.length);//convert bytes into a string
                //records = 1389293309*{5241=MWESIGWA MOSES_777777777_Kampala, 1348=MUGABI EMMANUEL_777777777_Mbarara}
                Vector guardsRecords = TourMIDlet.split(records, "*");
                //
                udateSignature = guardsRecords.elementAt(0).toString();
                //udateSignature = 1389293309
                String guardset = guardsRecords.elementAt(1).toString();
                //guardset = {5241=MWESIGWA MOSES_777777777_Kampala, 1348=MUGABI EMMANUEL_777777777_Mbarara}
                String locationsSet = guardsRecords.elementAt(2).toString();
                System.out.println("locationsSet"+locationsSet);
                String itemsSet = guardsRecords.elementAt(3).toString();
                //System.out.println(locationsSet);
                Vector guards = TourMIDlet.split(guardset.substring(1, guardset.length()-1),",");
                //above line removes {} and splits the remaining string by ','
                //guards = [5241=MWESIGWA MOSES_777777777_Kampala,  1348=MUGABI EMMANUEL_777777777_Mbarara]
                for(int i=0;i<guards.size();i++){//create Hashtable of guards
                    id_Guard = TourMIDlet.split(guards.elementAt(i).toString(),"=");
                    // id_Guard = [5241,MWESIGWA MOSES_777777777_Kampala]
                    guardHashtable.put((id_Guard.elementAt(0).toString()).trim(), (id_Guard.elementAt(1).toString()).trim());
                    id_Guard.removeAllElements();
                }
                Vector locations = TourMIDlet.split(locationsSet.substring(1, locationsSet.length()-1),",");
                //above line removes {} and splits the remaining string by ','
                //
                for(int i=0;i<locations.size();i++){
                    lctn_officer = TourMIDlet.split(locations.elementAt(i).toString(),"=");
                    locationsHashTable.put(lctn_officer.elementAt(0).toString().trim(), lctn_officer.elementAt(1).toString().trim());
                    lctn_officer.removeAllElements();
                }
                Vector items = TourMIDlet.split(itemsSet.substring(1, itemsSet.length()-1),",");
                //above line removes {} and splits the remaining string by ','
                //
                for(int i=0;i<items.size();i++){
                    id_item = TourMIDlet.split(items.elementAt(i).toString(),"=");
                    String name_unit = id_item.elementAt(1).toString().trim();
                    itemsHashtable.put(id_item.elementAt(0).toString().trim(), name_unit);
                    unitsVector.addElement(TourMIDlet.split(name_unit,"_").elementAt(1).toString());
                    id_item.removeAllElements();
                }
                guards.removeAllElements();
                locations.removeAllElements();
                items.removeAllElements();
                guardStoreIsEmpty = false;
            }
            else{
                guardStoreIsEmpty = true;
                udateSignature="0";
            }
            rs.closeRecordStore();
        }
        catch(Exception ex){
            //ex.printStackTrace();
            /*midlet.getAlert().setType(AlertType.ERROR);
            midlet.getAlert().setTitle("Application Error");
            midlet.getAlert().setString(ex.toString());
            midlet.switchDisplayable(null, midlet.getAlert());
            ex.printStackTrace();*/
        }
        System.out.println("Prev Locations"+locationsHashTable);
        return udateSignature;
    }
    
    private void loadGuardListFromFileSystem(){// called when the app fails to connect to server during update session
        try{
            InputStream strem = getClass().getResourceAsStream("/com/mob/util/guardList.xml");
            viewXML(strem);
            //get available phone file system roots e.g phone memory or memory card
            /*Vector roots = new Vector();
            Enumeration enume = FileSystemRegistry.listRoots();//enumeration of all storage spaces on ie phone memory,sd card
            while(enume.hasMoreElements()){
                roots.addElement(enume.nextElement());//adds storage media to roots vector
            }
            fc = (FileConnection)Connector.open("file:///"+roots.elementAt(roots.size()-1)+"guardList.xml",Connector.READ_WRITE);
            //the above line opens a connection to file on the memory card 'roots.size()-1' b'se sd card 'll the last element of roots vector if it exists
            try{
                fc.setReadable(true);
            }
            catch(IOException ex){
                midlet.switchDisplayable(null,midlet.getAlert());
                midlet.getAlert().setString("guardList.xml doesn't exist");
            }
            if (fc.exists()){//file exists
                fis = fc.openInputStream();//open up a stream on fc
                viewXML(fis);// pass strem to view xml
            }
            else{//file does not exist
                midlet.switchDisplayable(null,midlet.getAlert());
                midlet.getAlert().setString("guardList.xml doesn't exist");
            }*/
        }
        catch(Exception ex){
            //ex.printStackTrace();
        }
    }
    
    private void viewXML(InputStream ips){
        String locaction = new String();
        String action= new String();
        String id = new String();
        String number = new String();
        String gName = new String();
        String deploymentName = new String(),deploymentID = new String(),
                deploymentOfficerName = new String(),deploymentOfficerID = new String(),
                deploymentOfficerMobileNumber= new String(),officerDets,
                actn = new String(),itemID = new String(),itemName = new String(),itemUnit = new String();
        try{
            InputStreamReader xmlReader = new InputStreamReader(ips);
            XmlParser parser = new XmlParser( xmlReader );
            ParseEvent pe;
            parser.skip();//skips <?xml version='1.0' encoding='UTF-8'?>
            parser.read(Xml.START_TAG, null, "reply");//start tag <updateResp>
            //
            boolean trucking = true;
            while (trucking) {
            pe = parser.read();
            if (pe.getType() == Xml.START_TAG &&pe.getName().equals("dateTime")) {
                pe = parser.read();
                currentDateTime= pe.getText();
                System.out.println(currentDateTime);
            }
            else if(pe.getType() == Xml.START_TAG&&pe.getName().equals("signature")){
                pe = parser.read();
                upSignature = pe.getText();
                System.out.println(upSignature);
            }
            else if(pe.getType() == Xml.START_TAG&&pe.getName().equals("update")){
                String nemu = pe.getName();
                System.out.println("(1) = "+pe.getType());
                System.out.println("(1) = "+nemu);
                if (nemu.equals("update")) {
                    while((pe.getType() != Xml.END_TAG) || (pe.getName().equals(nemu) == false)){
                        parser.skip();
                        pe = parser.peek();
                        System.out.println("(2) = "+pe.getType());
                        System.out.println("(2) = "+pe.getName());
                        if (pe.getType() == Xml.START_TAG&&pe.getName().equals("record")){
                            String name1 = pe.getName();
                            if (name1.equals("record")) {
                                while ((pe.getType() != Xml.END_TAG) || (pe.getName().equals(name1) == false)) {
                                    pe = parser.read();
                                    if (pe.getType() == Xml.START_TAG &&pe.getName().equals("name")) {
                                        pe = parser.read();
                                        gName = pe.getText();
                                        //name = gName;
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("number")) {
                                        pe = parser.read();
                                        number = pe.getText();
                                        //phoneNum = number;
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("id")) {
                                        pe = parser.read();
                                        id = pe.getText();
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("action")) {
                                       pe = parser.read();
                                        action = pe.getText();
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("location")) {
                                        pe = parser.read();
                                        locaction = pe.getText();
                                    }
                                }
                                //hashtable for guards
                                if("INSERT".equalsIgnoreCase(action)){
                                    if(number==null||locaction==null){
                                        number = "null";
                                        locaction = "null";
                                    }
                                    guardHashtable.put(id.trim(), gName.trim()+"_"+number.trim()+"_"+locaction.trim());
                                }
                                else if("DELETE".equalsIgnoreCase(action)){
                                    guardHashtable.remove(id.trim());
                                }
                            }
                            else {
                                while ((pe.getType() != Xml.END_TAG) ||(pe.getName().equals(name1) == false))
                                    pe = parser.read();
                            }
                        }
                        else if(pe.getType() == Xml.START_TAG&&pe.getName().equals("deployment")){
                            System.out.println("(3) = "+pe.getType());
                            System.out.println("(3) = "+pe.getName());
                            String nam = pe.getName();
                            if (nam.equals("deployment")) {
                                while ((pe.getType() != Xml.END_TAG) || (pe.getName().equals(nam) == false)) {
                                    pe = parser.read();
                                    if (pe.getType() == Xml.START_TAG &&pe.getName().equals("deploymentName")) {
                                        pe = parser.read();
                                        deploymentName = pe.getText();
                                        System.out.println(deploymentName);
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("deploymentOfficerName")) {
                                        pe = parser.read();
                                        deploymentOfficerName = pe.getText();
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("deploymentOfficerMobileNumber")) {
                                        pe = parser.read();
                                        deploymentOfficerMobileNumber = pe.getText();
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("deploymentOfficerID")) {
                                        pe = parser.read();
                                        deploymentOfficerID = pe.getText();
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("action")) {
                                        pe = parser.read();
                                        action = pe.getText();
                                        //System.out.println(action);
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("deploymentID")) {
                                        pe = parser.read();
                                        deploymentID = pe.getText();
                                    }
                                }
                                //hashtable for locations
                                officerDets = deploymentOfficerName.trim()+"#"+deploymentOfficerID.trim()+"#"+deploymentOfficerMobileNumber.trim()+"#"+deploymentName.trim();
                                if("INSERT".equalsIgnoreCase(action)){
                                    locationsHashTable.put(deploymentID.trim(), officerDets);
                                }
                                else if("DELETE".equalsIgnoreCase(action)){
                                    locationsHashTable.remove(deploymentID.trim());
                                }
                            }
                            else {
                                while ((pe.getType() != Xml.END_TAG) ||(pe.getName().equals(nam) == false))
                                    pe = parser.read();
                            }
                        }
                        else if (pe.getType() == Xml.START_TAG&&pe.getName().equals("item")){
                            String atbNemu = pe.getName();
                            if (atbNemu.equals("item")) {
                                while((pe.getType() != Xml.END_TAG) || (pe.getName().equals(atbNemu) == false)){
                                    pe = parser.read();
                                    if (pe.getType() == Xml.START_TAG &&pe.getName().equals("itemID")) {
                                        pe = parser.read();
                                        itemID = pe.getText();
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("itemName")) {
                                        pe = parser.read();
                                        itemName = pe.getText();
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("itemUnit")) {
                                        pe = parser.read();
                                        itemUnit = pe.getText();
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("action")) {
                                        pe = parser.read();
                                        action = pe.getText();
                                    }
                                }
                                //hashtable for items
                                if("INSERT".equalsIgnoreCase(action)){
                                    if(itemID==null||itemName==null||itemUnit==null){
                                        itemUnit = "null";
                                        itemName = "null";
                                        itemID = "null";
                                    }
                                    unitsVector.addElement(itemUnit);//avoid duplicates
                                    itemsHashtable.put(itemID.trim(), itemName.trim()+"_"+itemUnit.trim());
                                }
                                else if("DELETE".equalsIgnoreCase(action)){
                                    itemsHashtable.remove(itemID.trim());
                                }
                            }
                            else{
                                while ((pe.getType() != Xml.END_TAG) ||(pe.getName().equals(atbNemu) == false))
                                    pe = parser.read();
                            }
                        }
                    }
                }
                else {
                    while ((pe.getType() != Xml.END_TAG) ||(pe.getName().equals(nemu) == false))
                    pe = parser.read();
                }
            }
            if (pe.getType() == Xml.END_TAG &&pe.getName().equals("reply"))
                trucking = false;
            }
            System.out.println("Guards Hashtable: "+guardHashtable.toString());
            System.out.println("Locations Hashtable: "+locationsHashTable.toString());
            System.out.println("Items Hashtable: "+itemsHashtable.toString());
            today = TourMIDlet.split(currentDateTime, " ").elementAt(0).toString();
            RecordStore.deleteRecordStore("guardRecords");//remove previous records
            byte [] byts = (upSignature+"*"+guardHashtable.toString()+"*"+locationsHashTable.toString()+"*"+itemsHashtable.toString()).getBytes();//get byte representation
            RecordStore rst = RecordStore.openRecordStore("guardRecords", true);
            rst.addRecord(byts, 0, byts.length);
            rst.closeRecordStore();//save update guard records
            midlet.id = pin;
            //Vector guardDetails = IGMS.split(guardHashtable.get(pin).toString(),"_");//split returned string by _
            //midlet.name = guardDetails.elementAt(0).toString();
            //System.out.println("main Menu");
            //midlet.switchDisplayable(null, midlet.mainMenu());
            /*if(firstUpdate == true){
                midlet.login();
                firstUpdate = false;
            }*/
        }
        catch (Exception ex) { 
            //ex.printStackTrace();
        }
    }
    
    class  UpdateTask  extends TimerTask{

        public void run() {
            try{
                if(upSignature==null){
                    /*midlet.getAlert().setType(AlertType.ERROR);
                    midlet.getAlert().setTitle("Connection Error");
                    midlet.getAlert().setString("Connection time out!");*/
                    //midlet.switchDisplayable(midlet.getAlert(),midlet.mainMenu());
                    if(guardStoreIsEmpty==true){
                        //loadGuardListFromFileSystem();
                    }
                    else{
                        //midlet.switchDisplayable(midlet.getAlert(), midlet.mainMenu());
                    }
                }
            }
            catch(Exception ex){
                
            }
            updateTimer.cancel();
        }  
    }
    
    private void updateTaskSheduler() {
        updateTimer = new Timer();
        try{
            updateTimer.schedule(new Update.UpdateTask(), 45000);
        }
        catch(IllegalStateException ex){
        }
        catch(IllegalArgumentException ex){
        }
    }
}
